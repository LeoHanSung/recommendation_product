package com.kebhana.recommendation.rest;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.kebhana.recommendation.dao.RecommendInfoDao;
import com.kebhana.recommendation.model.Hello;
import com.kebhana.recommendation.model.LoginInfo;
import com.kebhana.recommendation.model.PageOneOutput;
import com.kebhana.recommendation.model.RecommendInfo;
import com.kebhana.recommendation.model.UserInfo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Api(value="Recommend Service API")
@RestController
public class RecommendController {
		
	@Autowired
	private RecommendInfoDao recommendInfoDao;

	
	@ApiOperation(value="사용자 설문")
	@ApiImplicitParams({
		@ApiImplicitParam(name="name", value="이름 ",required = true, dataType="String", paramType="query")
	})
	@RequestMapping(value="/questionView", method=RequestMethod.POST)
	public ResponseEntity<PageOneOutput> geOtherService(
			@RequestBody LoginInfo loginInfo, 
			HttpServletRequest req) {
		
		log.info("questionView 시작");
		
		UserInfo userInfo = new UserInfo();
		PageOneOutput pageOneOutput = new PageOneOutput();
		
		
		//session에서 UserInfo 불러오기
		log.debug("세션 정보를 가져 옵니다. : ");
		userInfo = ((UserInfo)req.getSession().getAttribute("user"));
		

		log.info("데이터!!::",userInfo.toString());
		
		pageOneOutput.setUserInfo(userInfo); 
		
		log.info("questionView 끝");
		
		return new ResponseEntity<PageOneOutput>(pageOneOutput, HttpStatus.OK);	
	}

	

	@ApiOperation(value="사용자 정보 등록하기 ")
	@RequestMapping(value="/questionAnswer", method=RequestMethod.POST)
	public ResponseEntity <String > setUserInsert(
			@RequestBody RecommendInfo recommendInfo
		) throws Exception { 
		
		List<RecommendInfo> list = null;
		log.info("Start db insert");
		int re  = recommendInfoDao.insertInfo(recommendInfo);
		log.debug("result :"+ re);
		
		return new ResponseEntity<String> (re+"", HttpStatus.OK);
	}
	
}
