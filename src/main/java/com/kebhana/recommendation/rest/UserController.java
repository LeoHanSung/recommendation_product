package com.kebhana.recommendation.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kebhana.recommendation.dao.ProdStaticDao;
import com.kebhana.recommendation.dao.ProductInfoDao;
import com.kebhana.recommendation.dao.UserInfoDao;
import com.kebhana.recommendation.model.LoginInfo;
import com.kebhana.recommendation.model.PageFourInput;
import com.kebhana.recommendation.model.PageFourOutput;
import com.kebhana.recommendation.model.PageOneOutput;
import com.kebhana.recommendation.model.PageThreeOutput;
import com.kebhana.recommendation.model.ProdStatic;
import com.kebhana.recommendation.model.ProductInfo;
import com.kebhana.recommendation.model.UserInfo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Api(value = "User Service API")
@RestController
public class UserController {

	@Autowired
	private UserInfoDao userInfoDao;

	@Autowired
	private ProductInfoDao productInfoDao;
	
	@Autowired
	private ProdStaticDao prodStaticDao;
	
	@Autowired
    private RestTemplateBuilder restTemplateBuilder;



//	@ApiImplicitParams({
//		@ApiImplicitParam(name="name", value="이름 ",required = true, dataType="String", paramType="query")
//	})
//	@RequestMapping(value="/hello", method=RequestMethod.GET)
//	public Hello getHelloMsg(@RequestParam(value="name" ,required = true) String name ) {
//		return new Hello(vistorConouter.incrementAndGet(), String.format(msgTemplate, name));
//	}

	@ApiOperation(value = "고객 정보 가져와서 연령&성별 상품, Best 상품 목록")
//	@ApiImplicitParams({
//		@ApiImplicitParam(name="userId", value="아이디 ",required = true, dataType="String", paramType="query")
//
//	})
	@RequestMapping(value = "/userInfo", method = RequestMethod.POST)
	public ResponseEntity<PageOneOutput> geOtherService(@RequestBody LoginInfo loginInfo) {
		log.info("UserController 시작");
		log.info(loginInfo.getUserId());
		UserInfo userInfo = new UserInfo();
//		userInfo.setUserId("1234123");
		userInfo.setUserId(loginInfo.getUserId());
		PageOneOutput pageOneOutput = new PageOneOutput();
		try {
			// id로 고객정보 찾기
			log.info("Start productinfo_db select");
			userInfo = userInfoDao.selectUserById(userInfo);

			// id정보의 성별과 나이로 추천상품 찾기.
			userInfo.getAge();
			List<ProductInfo> genAgeProduct = productInfoDao.selectProductGenderAge(userInfo);
			pageOneOutput.setGenAgeProduct(genAgeProduct);

			// 은행 추천 상품 찾기.
			List<ProductInfo> bestProduct = productInfoDao.selectBestProduct();
			pageOneOutput.setBestProduct(bestProduct);
			// userInfo설정
			pageOneOutput.setUserInfo(userInfo);

			log.info("end of seleting db");
		} catch (Exception e) {
			// TODO: handle exception
		}
		return new ResponseEntity<PageOneOutput>(pageOneOutput, HttpStatus.OK);
	}

//	@ApiOperation(value = "page3 고객정보 가지고오기")
//	@ApiImplicitParams({
//			@ApiImplicitParam(name = "userId", value = "아이디 ", required = true, dataType = "String", paramType = "query")
//
//	})
//	@RequestMapping(value = "/page3", method = RequestMethod.GET)
//	public ResponseEntity<PageOneOutput> getPageThree(@RequestParam(value="userId" ,required = true) String userId ) {
//
//		log.info("page3 Controller 시작");
//		PageOneOutput pageOneOutput = new PageOneOutput();
//		return new ResponseEntity<PageOneOutput>(pageOneOutput, HttpStatus.OK);
//	}

	@ApiOperation(value = "page3 결과물 뿌려주기")
	@RequestMapping(value = "/page3", method = RequestMethod.POST)
	public ResponseEntity<PageThreeOutput> getPageThree(
			@RequestBody LoginInfo loginInfo
			) {
		log.info("page3 Controller 시작");
		System.out.println("고객id 들어와?:"+loginInfo.getUserId());
		UserInfo userInfo = new UserInfo();
		String userId = loginInfo.getUserId();
		
		userInfo.setUserId(userId);

		try {
			userInfo = userInfoDao.selectUserById(userInfo);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		PageThreeOutput pageThreeOutput = new PageThreeOutput();
		
		
		ArrayList<String> dataAnalProdCod = new ArrayList<String>();
		
		//초기고정
//		dataAnalProdCod.add("S001");
//		dataAnalProdCod.add("S002");
//		dataAnalProdCod.add("S003");
//		pageThreeOutput.setDataAnalProdCod(dataAnalProdCod);	
		
		//유저 정보 셋팅
		int age = userInfo.getAge();
		int creLvl = userInfo.getCreLvl();
		int fundCnt = userInfo.getFundCnt();
		int loanCnt = userInfo.getLoanCnt();
		int annual = userInfo.getAnnual()/1000;
		int gender = userInfo.getGender();
		
		String url = "http://169.56.170.168:8085/recommend?userAttr="+age+","+creLvl+","+fundCnt+","+loanCnt+","+annual+","+gender;
		
		RestTemplate restTemplate = restTemplateBuilder.build();

		String result = restTemplate.getForObject(url, String.class);
//		String result = restTemplate.getForObject("http://169.56.170.168:8085/recommend?userAttr=20,1,3,2,5,0", String.class);
		
		ObjectMapper mapper = new ObjectMapper();
	    Map<String, Object> map = new HashMap<String, Object>();
	    try {
	    	map = mapper.readValue(result, new TypeReference<Map<String, Object>>(){});
	    }catch(Exception e) {
	    	e.printStackTrace();
	    }
	    
	    
	    //상품정보 설정부분
	    int prodNum = ((List) map.get("prodList")).size();
	    log.info("상품개수:"+prodNum);
	    log.info("상품입력 시작");
	    if(prodNum==0) {
	    	dataAnalProdCod.add("S003");
	    	dataAnalProdCod.add("S011");
	    	dataAnalProdCod.add("S015");
	    	log.info("0개"+dataAnalProdCod.toString());
	    }
	    else if(prodNum==1) {
	    	dataAnalProdCod.addAll((List)map.get("prodList"));
	    	dataAnalProdCod.add("S003");
	    	dataAnalProdCod.add("S011");
	    	log.info("1개"+dataAnalProdCod.toString());
	    }else if(prodNum==2) {
	    	dataAnalProdCod.addAll((List)map.get("prodList"));
	    	dataAnalProdCod.add("S003");
	    	log.info("2개"+dataAnalProdCod.toString());
	    }else if(prodNum>=3) {
	    	dataAnalProdCod.addAll(((List) map.get("prodList")).subList(0, 3));
	    	log.info("3개이상"+dataAnalProdCod.toString());
	    }
	    
	    
		pageThreeOutput.setDataAnalProdCod(dataAnalProdCod);
		log.info("pageThreeOutput끝나"+pageThreeOutput);

		return new ResponseEntity<PageThreeOutput>(pageThreeOutput, HttpStatus.OK);
	}
	
	@ApiOperation(value = "page4 고객정보 가지고오기")
	@RequestMapping(value = "/page4", method = RequestMethod.POST)
	public ResponseEntity<PageFourOutput> getPageThree(
			@RequestBody PageFourInput pageFourInput
			) {
		log.info("page4 Controller 시작");
		String prodCode = pageFourInput.getProdCode();
		log.info("상품코드"+prodCode);
		
		PageFourOutput pageFourOutput = new PageFourOutput();
		try {
			log.info("DB조회 시작");
			List<ProdStatic> prodGroupByGender =  prodStaticDao.prodGroupByGender(prodCode);
			List<ProdStatic> prodGroupByAgeBand = prodStaticDao.prodGroupByAgeBand(prodCode);
			List<ProdStatic> prodGroupByAnnual = prodStaticDao.prodGroupByAnnual(prodCode);
			List<ProdStatic> prodGroupByCredLVL = prodStaticDao.prodGroupByCredLVL(prodCode);
			List<ProdStatic> prodGroupByFundCnt = prodStaticDao.prodGroupByFundCnt(prodCode);
			List<ProdStatic> prodGroupByLoanCnt = prodStaticDao.prodGroupByLoanCnt(prodCode);
			
			// 
			log.info("값 조회 안돼?"+prodGroupByGender);
			
			log.info("OUTPUT 값 세팅");
			pageFourOutput.setProdGroupByGender(prodGroupByGender);
			pageFourOutput.setProdGroupByAgeBand(prodGroupByAgeBand);
			pageFourOutput.setProdGroupByAnnual(prodGroupByAnnual);
			pageFourOutput.setProdGroupByCredLVL(prodGroupByCredLVL);
			pageFourOutput.setProdGroupByFundCnt(prodGroupByFundCnt);
			pageFourOutput.setProdGroupByLoanCnt(prodGroupByLoanCnt);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.info("출력값: "+pageFourOutput);
		log.info("PAGE4 OUT!!!");
		return new ResponseEntity<PageFourOutput>(pageFourOutput, HttpStatus.OK);
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/useModel", method = RequestMethod.GET)
	public ResponseEntity<Map> py()
	{
		RestTemplate restTemplate = restTemplateBuilder.build();

		String result = restTemplate.getForObject("http://169.56.170.168:8085/recommend?userAttr=20,1,3,2,5,0", String.class);
		
		ObjectMapper mapper = new ObjectMapper();
	    Map<String, Object> map = new HashMap<String, Object>();
	    try {
	    	map = mapper.readValue(result, new TypeReference<Map<String, Object>>(){});
	    }catch(Exception e) {
	    	e.printStackTrace();
	    }

		HttpHeaders headers = new HttpHeaders();
	    headers.add(HttpHeaders.CONTENT_TYPE, "application/json; charset=UTF-8");

	    return new ResponseEntity<Map>(map, headers, HttpStatus.OK);
	}
}
