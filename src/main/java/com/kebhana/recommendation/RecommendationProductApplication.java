package com.kebhana.recommendation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RecommendationProductApplication {

	public static void main(String[] args) {
		SpringApplication.run(RecommendationProductApplication.class, args);
	}

}
