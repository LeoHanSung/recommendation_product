package com.kebhana.recommendation.model;

import lombok.Data;

@Data
public class ProductInfo {
	 private String productCode;		//상품
	 private String  productName 		; // 상품명
	 private String  interestRatio 		; // 이자율
	 private int  age 		    ; // 연령 
	 private String  gender 	    ; // 성별 
	 private String  mnthMaxMny    ; // 월 최대 납입금액
	 private String  wageTransfer 	    ; // 급여이체 여부 
	 private String  telPymntDirDbt 	    ; // 통신비 자동이체 여부 
	 private String  aptExpenDirDbt 	    ; // 아파트 관리비 자동이체 여부
	 private String  overThreeYearTrans 	    ; // 3년 이상 여부 
	 private String  marketingAgree 	    ; // 마케팅 동의 여부 
	 private String  usingHanacard 	    ; // 하나카드 사용 여부 
	 private String bestOption;			  // 추천상품으로 보여줄 것들
	 private int cnt;

}
