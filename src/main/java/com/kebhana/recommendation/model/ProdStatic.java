package com.kebhana.recommendation.model;

import lombok.Data;

@Data
public class ProdStatic {
	int gender;
	int ageBand;
	int creLvl;
	int loanCnt;
	int fundCnt;
	int annual;
	int cnt;
	
}
