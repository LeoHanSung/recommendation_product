package com.kebhana.recommendation.model;

import java.util.List;

import lombok.Data;

@Data
public class PageOneOutput {
	private UserInfo userInfo; // 사용자 정보
	private List<ProductInfo> genAgeProduct; // 상위 3개
	private List<ProductInfo> bestProduct; // 베스트상품 3개
}
