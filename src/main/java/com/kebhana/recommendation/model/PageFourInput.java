package com.kebhana.recommendation.model;

import lombok.Data;

@Data
public class PageFourInput {
	 private String userId 		; // 사용자 아이디
	 private String prodCode		; // 상품코드
}
