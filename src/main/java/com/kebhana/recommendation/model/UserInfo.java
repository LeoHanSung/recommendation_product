package com.kebhana.recommendation.model;

import java.util.Calendar;

import lombok.Data;

@Data
public class UserInfo {
	
	
	
		/*
		 	USER_ID,
		    PW,
		    USER_NAME,
		    GENDER,
		    BIRTHDT,
		    WAGE_TRANSFER,
		    TEL_PYMNT_DIR_DBT,
		    APT_EXPEN_DIR_DBT,
		    OVER_THREE_YEAR_TRANS,
		    MARKETING_AGREE,
		    USING_HANACARD
		 */
	 private String  userId 		; // 사용자 아이디
	 private String pw;
	 private String  userName 		; // 사용자 이름
	 private int	 gender 		; // 성별 코드
	 private String  birthdt 	    ; // 생년 월일 
	 private String wageTransfer	; //급여이체 여부
	 private String telPymntDirDbt; // 통신비 자동이체 여부
	 private String aptExpenDirDbt; // 아파트 관리비 자동이체 여부
	 private String overThreeYearTrans;	//3년 이상 자동이체 여부
	 private String marketingAgree;// 마케팅 동의 여부
	 private String usingHanacard;	// 하나카드 사용 여부
	 private int creLvl;
	 private int loanCnt;
	 private int fundCnt;
	 private int annual;
	 
	 
	 private int age			; //나이
	 private String genName			; // 성별 이름
	 private int ageBand			; //연령대
	 
	 /**
	  * 만 나이 계산하는 getter
	  * @return
	  */
	 public int getAge() {
		 // 만 나이 계산
		 Calendar current = Calendar.getInstance();
		 int currentYear = current.get(Calendar.YEAR);
		 int currentMonth = current.get(Calendar.MONTH);
		 int currentDay = current.get(Calendar.DAY_OF_MONTH);
		 
		 
		 int birthYear = Integer.parseInt(birthdt.substring(0, 4));
		 int birthMonth = Integer.parseInt(birthdt.substring(4,6));
		 int birthDay = Integer.parseInt(birthdt.substring(6,8));
		 
		 this.age = currentYear-birthYear;
		 if(birthMonth*100+birthDay>currentMonth*100+currentDay) {
			 this.age--;
		 }
		 return this.age;
	 }
	
	 //성별 문자화
	 public String getGenName() {
		 if(this.gender==0) {
			 this.genName = "남성";
		 }else {
			 this.genName = "여성";
		 }
		 return this.genName;
	 }
	 
	 //연령대 계산
	 public int getAgeBand() {
		 if(this.age >=20 && this.age<30) {
			 this.ageBand = 20;
		 }else if(this.age >=30 && this.age<40) {
			 this.ageBand = 30;
		 }else if(this.age >=40 && this.age<50) {
			 this.ageBand = 40;
		 }else {
			 this.ageBand = 50;
		 }
		 return this.ageBand;
	 }
}
