package com.kebhana.recommendation.model;

import java.util.List;

import lombok.Data;

@Data
public class PageFourOutput {
	List<ProdStatic> prodGroupByGender;
	List<ProdStatic> prodGroupByAgeBand;
	List<ProdStatic> prodGroupByAnnual;
	List<ProdStatic> prodGroupByCredLVL;
	List<ProdStatic> prodGroupByFundCnt;
	List<ProdStatic> prodGroupByLoanCnt;
	
}
