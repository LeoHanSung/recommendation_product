package com.kebhana.recommendation.model;

import lombok.Data;

@Data
public class RecommendInfo {
	 private String  serialNo 			; // 일련번호
	 private String  userid 			; // 사용자ID
	 private String  inputDatetime 		; // 입력시간 
	 private String  moneyPurpose 	    ; // 자금목적 
	 private String  period    			; // 가입기간
	 private String  mthlyPymntAmnt 	; // 월납입금액 
	 private String  wageTransfer 	    ; // 급여이체여부
	 private String  telPymntDirDbt 	; // 통신비자동이체여부 
	 private String  aptExpenDirDbt 	; // 아파트관리비자동이체여부 
	 private String  overThreeYearTrans ; // 3년이상거래여부 
	 private String  marketingAgree 	; // 마케팅 동의 여부 
	 private String  usingHanacard 	    ; // 하나카드 사용 여부 
	 

}
