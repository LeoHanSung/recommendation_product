package com.kebhana.recommendation.dao;



import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import com.kebhana.recommendation.model.RecommendInfo;





@Mapper
public interface RecommendInfoDao {

	/**
	 * 사용자 등록하기 
	 * @param recommendInfo
	 * @return
	 * @throws Exception
	 */
	int insertInfo(RecommendInfo recommendInfo) throws Exception;
		
	
}
			