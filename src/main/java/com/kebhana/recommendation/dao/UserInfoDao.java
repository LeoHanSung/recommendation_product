package com.kebhana.recommendation.dao;

import org.apache.ibatis.annotations.Mapper;

import com.kebhana.recommendation.model.UserInfo;


@Mapper
public interface UserInfoDao {
	
	UserInfo selectUserById(UserInfo userInfo) throws Exception;		
	
}
