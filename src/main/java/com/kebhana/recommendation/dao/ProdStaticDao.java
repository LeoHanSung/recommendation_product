package com.kebhana.recommendation.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.kebhana.recommendation.model.ProdStatic;


@Mapper
public interface ProdStaticDao {
	
	List<ProdStatic> prodGroupByGender(String prodCode) throws Exception;
	List<ProdStatic> prodGroupByAgeBand(String prodCode) throws Exception;
	List<ProdStatic> prodGroupByCredLVL(String prodCode) throws Exception;
	List<ProdStatic> prodGroupByAnnual(String prodCode) throws Exception;
	List<ProdStatic> prodGroupByFundCnt(String prodCode) throws Exception;
	List<ProdStatic> prodGroupByLoanCnt(String prodCode) throws Exception;
	
}
