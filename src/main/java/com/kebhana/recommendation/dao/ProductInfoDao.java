package com.kebhana.recommendation.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.kebhana.recommendation.model.ProductInfo;
import com.kebhana.recommendation.model.UserInfo;


@Mapper
public interface ProductInfoDao {
	/**
	 * 상품 통계 갖고오기
	 * @return
	 * @throws Exception
	 */
	List<ProductInfo> selectProductGenderAge(UserInfo userInfo) throws Exception;		
	
	List<ProductInfo> selectBestProduct() throws Exception;	
}
