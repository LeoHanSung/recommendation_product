# recommendation-product backend service

recommendation-product 백엔드서비스로 스프링 프레임워크로 개발한다.
   

#### 로컬 git 저장소 초기화
로컬 개발환경을 초기화 한다.
```bash
    mkdir 프로젝트 작업공간 생성
    cd 만든 작업 공간으로 이동
    git clone https://gitlab.com/jinlpin/pengsoo.git
```

#### git 명령어
로컬의 파일을 수정하고 커밋하기 위해 자주 사용되는 git 명령어이다.   
수정된 파일을 저장하기 위해 변경한 파일을 스테이징 상태로 변경하고 로컬 저장소에 커밋을 한다.  
로컬 저정소에 커밋된 파일은 일괄적으로 원격 저장소에 push한다.
```bash
# 개발전 수행
git pull # 원격 저장소의 정보를 로컬 저장소와 동기화 한다.

# 개발후 수행
git add file_name  # fine_name 을 스테이징 상태로 변경한다.  스테이징 된 상태만  commit 할 수 있다.
git commit -m '수정된 내용을 기록해 주세요'  # 로컬저장소에 변경된 소스를 commit 한다.
git push origin master  # local의 저장소를 원격 저장소로 push 한다.
```
### 기타
---
#### DB / 로그 설정정보 변경
application.yml 파일 정보의 설정 정보를 변경한다. database의 접속 정보 또는 로그 출력 정보를 수정한다.
```yaml
  datasource:
    driverClassName: com.mysql.jdbc.Driver
    url: jdbc:mysql://169.56.170.169:30004/recommedationdb # database 접속 정보 수정 
    username: recommendation # db접속 계정 정보변경 
    password: "passw0rd"
```


#### 디렉토리 구조
msa 프로젝트는 다음과 같은 디렉토리 구조를 갖는다.
```code
src
    main
        java
            msa~project~root/config // 환경 설정 정보 위치 
            msa~project~root/rest   // rest api controller 위치
            msa~project~root/service    // 서비스 파일 위치
            msa~project~root/dao        // dao 파일 위치
            msa~project~root/model      // vo 객체 위치 
        resources // 어플리케이션 yaml 파일 위치 
            mybatis
                mapper // database xml 위치 
```


#### database 생성하기    
msa 를 원하는 명으로 치환 한다.  
```sql
CREATE DATABASE recommedationdb DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
CREATE USER 'recommendation'@'%' IDENTIFIED BY 'passw0rd';
GRANT ALL PRIVILEGES ON recommendationdb.* TO 'recommendation'@'%';
```




